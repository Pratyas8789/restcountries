import { useState } from 'react'
import NavBar from './components/NavBar'
import SearchBar from './components/SearchBar'

function App() {
  const [mode, setMode] = useState("light");
  const handleTheme = () => {
    if (mode === "light") {
      setMode("dark");
    } else {
      setMode("light");
    }
  }

  return (
    <div className="App">
      <NavBar changeTheme={handleTheme} mode={mode} />
      <SearchBar mode={mode} />
    </div>
  )
}

export default App;
