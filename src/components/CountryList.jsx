import React from 'react'
import data from "./countries.json"
import UpdatedCountry from './UpdatedCountry.jsx'

export default function CountryList(props) {
    const { countryName, region } = props;
    const countryByRegion = data.filter(country => country.region == region);
    const countryByName = data.filter(country => (country.name.common).includes(countryName));

    return (
        <div className='CountryList'>
            {region !== "" ? <UpdatedCountry mode={props.mode} search={countryByRegion} /> : ""}
            {countryName !== "" ? <UpdatedCountry mode={props.mode} search={countryByName} /> : ""}
            {countryName === "" && region === "" && <UpdatedCountry mode={props.mode} search={data} />}
        </div>
    )
}
