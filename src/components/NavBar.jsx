import React from 'react'

export default function NavBar(props) {
  let theme;
  if (props.mode === "dark") {
    theme = {
      color: 'white',
      backgroundColor: '#202D36',
    }
    document.body.style.backgroundColor = "#2B3743"
  } else {
    theme = {
      color: 'black',
      backgroundColor: 'white',
    }
    document.body.style.backgroundColor = "white"
  }

  return (
    <div style={theme} className='NavBar'>
      <h2>Where in the world? </h2>
      <h3 onClick={props.changeTheme} className="changeTheme" > <i className="fa fa-moon-o" aria-hidden="true"></i>Dark Mode</h3>
    </div>
  )
}
