import React from 'react'
import data from "./countries.json"
import CountryList from './CountryList'

export default function SearchBar(props) {
    const [countryName, setCountryName] = React.useState("")
    const [region, setRegion] = React.useState("")
    const handleSearchCountry = (event) => {
        let countryName = event.target.value;
        countryName = countryName.charAt(0).toUpperCase() + countryName.substring(1).toLowerCase();
        setCountryName(countryName);
        setRegion("");
        document.getElementById("option").value = "";
    }

    const handleSelect = (event) => {
        setRegion(event.target.value);
        setCountryName("");
        document.getElementById("search").value = "";
    }

    const allRegions = {};
    data.map((country) => allRegions[country.region] = 0);

    const optionForRegion = Object.keys(allRegions).map((region, index) => {
        return (
            <option value={region} key={index}>{region}</option>
        )
    })

    let theme;
    if (props.mode === "dark") {
        theme = {
            color: 'white',
            backgroundColor: '#202D36',
        }
    } else {
        theme = {
            color: 'black',
            backgroundColor: 'white',
        }
    }

    return (
        <>
            <div className='SearchBar'>
                <div style={theme} className="search">
                    <i className="fa fa-search fa-2x" aria-hidden="true"></i>
                    <input style={theme} type="search" name="" id="search" onChange={handleSearchCountry} placeholder='Search for a country...' />
                </div>
                <div className="option">
                    <select style={theme} onClick={handleSelect} className='selectOption' name="" id="option">
                        <option defaultValue value="">Filter by Region</option>
                        {optionForRegion}
                    </select>
                </div>
            </div>
            <CountryList mode={props.mode} countryName={countryName} region={region} />
        </>
    )
}
