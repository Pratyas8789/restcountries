import React from 'react'

export default function UpdatedCountry(props) {
    const updatedCountry = (props.search).map((country, index) => {

        let theme;
        if (props.mode === "dark") {
            theme = {
                color: 'white',
                backgroundColor: '#2B3743',
            }
        } else {
            theme = {
                color: 'black',
                backgroundColor: 'white',
            }
        }

        return (
            <div style={theme} key={index} className="card">
                <div className="flag">
                    <img src={country.flags.png} alt="" />
                </div>
                <div className="CountryName">
                    <h3 >{country.name.common}</h3>
                </div>
                <div className="CountryDetails">
                    <h3>Population:{country.population}</h3>
                    <h3>Region:{country.region}</h3>
                    <h3>Capital: {country.capital}</h3>
                </div>
            </div>
        )
    })
    return (
        <>
            {updatedCountry}
        </>

    )
}
